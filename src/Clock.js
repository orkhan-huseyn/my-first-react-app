import React from 'react';
import './Clock.css';

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
    };
    console.log('constructor() called');
  }

  shouldComponentUpdate() {
    console.log('shouldComponentUpdate() called');
    return true;
  }

  componentDidMount() {
    console.log('componentDidMount() called');
    setInterval(() => {
      this.setState({
        date: new Date(),
      });
    }, 1000);
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate() called');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount() called');
  }

  render() {
    console.log('render() called');
    return (
      <div className="box">
        <h3>This is the clock!</h3>
        <span>{this.state.date.toLocaleTimeString()}</span>
      </div>
    );
  }
}

export default Clock;

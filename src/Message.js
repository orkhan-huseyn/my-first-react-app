import React from 'react';

// camelCase
// PascalCase
// kebab-case
// snake_case

class Message extends React.Component {
  constructor(props) {
    super(props);
    console.log('constructor() called');
  }

  componentDidMount() {
    console.log('componentDidMount() called');
  }

  render() {
    console.log('render() called');
    return <h1>{this.props.message}</h1>;
  }
}

export default Message;

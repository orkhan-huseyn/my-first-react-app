import React from 'react';
import ReactDOM from 'react-dom';
import Pet from './Pet';
import Clock from './Clock';

function App() {
  const pets = [
    {
      name: 'Your Dog',
      breed: 'Golden Retriever',
    },
    {
      name: 'My Dog',
      breed: 'Labrador',
    },
    {
      name: 'His Dog',
      breed: 'Coban Iti',
    },
  ];

  const petList = pets.map((pet, index) => {
    return <Pet key={index} name={pet.name} breed={pet.breed} />;
  });

  return (
    <div id="app">
      {petList}
      <Clock />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));

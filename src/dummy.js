// named export
export function add(x, y) {
  return x + y;
}

export function multiply(x, y) {
  return x * y;
}

// default export
export default function subtract(x, y) {
  return x - y;
}

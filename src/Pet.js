import styles from './Pet.module.css';

export default function Pet({ name, breed }) {
  return (
    <div className={styles.box}>
      <h3>{name}</h3>
      <span>{breed}</span>
    </div>
  );
}
